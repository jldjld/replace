import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthComponent } from './auth.component';
import { User } from '../entity/user';
import { DebugElement } from '@angular/core';
import { By } from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';



describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;
  let submitEl: DebugElement;
  let loginEl: DebugElement;
  let passwordEl: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthComponent ],
      imports: [ FormsModule, HttpClientTestingModule, RouterTestingModule ]})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    submitEl = fixture.debugElement.query(By.css('button'));
    loginEl = fixture.debugElement.query(By.css('input[type=email]'));
    passwordEl = fixture.debugElement.query(By.css('input[type=password]'));
  
  });


  it('should login on the app', () => {
    let user:User;
    loginEl.nativeElement.value = "test@test.test";
    passwordEl.nativeElement.value = "test";

    component.loggedIn.subscribe((value) => user = value);

    submitEl.triggerEventHandler('click', null);
    
    expect(user.email).toBe("test@test.test");
    expect(user.password).toBe("test");
  });
});
