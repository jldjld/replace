import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../entity/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  @Output() loggedIn = new EventEmitter<User>();
  @Input() enabled = true;
sessionId: Observable<string>;
token: Observable<string>;
  email:string;
  password: string;
  user: User = {
    email: null,
    password: null,
    favorites:null
  };
  repeat: string = null;
  message:string;
  
    constructor(private authService: AuthService,private router:Router) { }
  
    ngOnInit() {
  }
      
    // function using the authservice for user login 
    login() {
      this.authService.login(this.email, this.password)
      .subscribe(user =>{ 
      this.router.navigate(['/home'])
      });
    }
    // function using the authservice to add a new user

    register() {
      if (this.validForm()) {
        this.authService.addUser(this.user).subscribe(() => this.message = 'Successfully registered !');
      }
    }
    // function used to validate the user form when registering a new user
    validForm():boolean {
      return this.user.email && this.user.password && this.user.password === this.repeat;
    }
  }
