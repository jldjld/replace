import { Pipe, PipeTransform } from '@angular/core';
import { Product } from './entity/product';

@Pipe({
  name: 'searchCat'
})
export class SearchCatPipe implements PipeTransform {
  // If an argument(written by the user) is matching a product category, 
  // return the matching value
  transform(values: Product[], args?: any): any {

    if(!args){
      return values;
    }
    return values.filter((item) => item.category === args);
    
  }

}
