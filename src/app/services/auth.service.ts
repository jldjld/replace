import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/user';import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = 'http://localhost:8000/api';
  router: Router;
  public user: BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  // Function used to add a new user to the database, using http POST request
  addUser(user: User) {
    return this.http.post<User>("http://localhost:8000/api/register", user);
  }
  // Function used to login on the app, the pipe is storing the Token in localstorage
  // switchmap is using function getUser to get the user informations in the database.
  login(username: string, password: string) {
    return this.http.post<{ token: string }>("http://localhost:8000/api/login_check", {
      username,
      password
    }).pipe(
      tap(response => localStorage.setItem('token', response.token)),
      switchMap(() => this.getUser())
    );
  }
  // Function used to log out, the local storage, will delete the users token
  logout() {
    localStorage.removeItem('token');
    this.user.next(null);

  }
  // getUser is used to find matching users information in the database, when login on the app
  getUser(): Observable<User> {
    return this.http.get<User>(this.url).pipe(

      tap(user => this.user.next(user))
    );
  }
}
