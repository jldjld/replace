import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'replace';

  
  constructor(private authService: AuthService, router: Router) {
}
ngOnInit(){
  
  this.authService.getUser().subscribe(() => null, () => null);
  
}
}
