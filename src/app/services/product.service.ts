import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';
import { Params } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }
  // Function used to find all products in the product database on server side
  // using http request GET

  findAll(): Observable<Product[]> {
    return this.http.get<Product[]>('http://localhost:8000/api/product');
  }
  // Function used to filter sustainable products in the product database on server side
  // using http request GET

  findSustainableProduct(): Observable<Product[]> {
    return this.http.get<Product[]>('http://localhost:8000/api/product/true');
  }
  // Function used to add a new product in the product database on server side
  // using http request POST
  add(product:Product): Observable<Product>{
    return this.http.post<Product>('http://localhost:8000/api/product', product);
  }
  // Function used to filter products by name in the product database on server side
  // using http request GET
  findByName(search:string): Observable<Product[]>{
    return this.http.get<Product[]>('http://localhost:8000/api/product/search/'+ search);

  } 
}
