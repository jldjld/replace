export interface Product {
    id?:number;
    name:string;
    description:string;
    bpImgPath:string;
    altImgPath:string;
    sustainable?:boolean;
    category?:string;
    usingTime:number;
    decayingTime:number;
    lifeTime:number;
    alternatives:[];
}