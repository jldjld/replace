const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')('http');

app.get('/results/', (req, res) => {
    res.send('<h1>hello chart</h1>');
});

http.listen(3000, () => {
    console.log('port 4200');
});

io.on('connection', (socket) => {
    console.log('client connected');
});