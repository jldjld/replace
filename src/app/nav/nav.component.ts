import { Component, OnInit } from '@angular/core';
import { User } from '../entity/user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  user:User;

  constructor(private authService: AuthService, private router:Router) { }
  ngOnInit() {
   this.authService.user.subscribe(data => this.user= data)
  }
  // Function used to logout the user using authService and navigate to the home component
  logout(){
    this.authService.logout();
    this.router.navigate(['/home']);
  }

}
