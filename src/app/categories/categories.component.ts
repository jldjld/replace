import { Component, OnInit } from '@angular/core';
import { Product } from '../entity/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  category:string = '';
  products:Product[] = [];
  product:Product = {
    name:'',
    description:'',
    bpImgPath:'',
    altImgPath:'',
    sustainable:true,
    category:'',
    usingTime:0,
    decayingTime:0,
    lifeTime:0,
    alternatives:[]
  };

  constructor(private repo:ProductService) { }

  ngOnInit() {
    this.repo.findSustainableProduct().subscribe(products => this.products =products);
    
  }

}
