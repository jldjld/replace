# Partie back-end du projet

https://gitlab.com/jldjld/replace_back

# **REPLACE**

**<u>CONTEXTE</u>**

Dans le cadre de ma formation de développeur web et web mobile à Simplon, il m'était demandé de trouver une idée innovante, pour réaliser un projet chef-d'oeuvre, validant toutes les compétences néscessaires au passage du titre professionel.



<u>**PRÉSENTATION DU PROJET**</u>

Je suis partie d'un constat, dans ma vie quotidienne je souhaite réduire ma conssomation de produits à usage unique. Du fait des lois à venir, pour interdire des produits à la conssomation, je souhaitais avoir un outil pour aider les particuliers, ou aider les entreprises à trouver des alternatives plus durables et éco-responsable.

J'ai donc imaginé développer une fonctionnalité, pour mettre en parallèle la durée d'utilisation d'un produit, face à sa durée de décomposition dans l'environnement. Cet outil servirait ensuite de tremplin pour mettre en avant les alternatives existantes, qui permettrait de réduire ou faire disparaitre les déchets.



<u>**USER STORIES**</u>

- je suis chef d’entreprise, et je souhaites adopter une politique d’entreprise plus éco-responsable, pour l’achat de fournitures, et de matériel de bureau.

- je suis futur entrepreneur, et je souhaites intégrer les principes zéro-déchet à mon entreprise, pour investir sur du matériel qui puisse subsister à long terme.

- je suis chef d’entreprise, et je souhaites adopter une politique d’entreprise plus éco-responsable, pour valoriser mon image de marque auprès de ma clientèle.

- je suis responsable des achats au sein d’une PME, et je souhaites sensibiliser ma hiérarchie à l’achat de produits éco-responsable, pour anticiper les futurs lois à venir concernant l’interdiction de produits à usage unique.

<u>**USE CASE**</u>

![usecase_replace](/conception/usecase_replace.png)

<u>**Diagramme de classes**</u>

![ClassDiagramreplace-v2](/conception/ClassDiagramreplace-v2.png)

<u><u>**Maquettes**</u><u>



![1-authentification](conception/1-authentification.jpg)

![1-authentification](conception/2-Homepage.jpg)
![1-authentification](conception/5-page-produit-2.jpg)
![1-authentification](conception/13-cat-gories-mobile.jpg)

![1-authentification](conception/desktop replace.mp4)
![1-authentification](conception/replace-w1.mp4)


