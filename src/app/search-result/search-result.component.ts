import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../entity/product';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})

export class SearchResultComponent implements OnInit {
  search:string;
  products:Product[] = [];
  product: Product[];
  chart: [];

  // Properties used to load a chart on the search-result page
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels = ['décomposition', 'utilisation'];
  public barChartType = 'doughnut';
  public barChartLegend = true;

  public barChartData = [
    {data: [99, 10], borderColor:'#87ccd1', backgroundColor:['#461411', '#05a809'],borderWidth:'5', label: 'Series A'}
  ];

  constructor(private repo:ProductService, private router:ActivatedRoute) { }

  ngOnInit() {
  //On init get search parameter with findByName function from the product service, subscribe will assign this param with an existing product
    let search = this.router.snapshot.paramMap.get('search');
    this.repo.findByName(search).subscribe(products => this.products = products);
    

  }

}
