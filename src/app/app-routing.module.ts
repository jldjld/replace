import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthComponent } from './auth/auth.component';
import { CategoriesComponent } from './categories/categories.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { TestimoniesComponent } from './testimonies/testimonies.component';

// Here I define route names for each components
const routes: Routes = [
  {path:"home", component: HomePageComponent},
  {path:"", component: AuthComponent},
  {path:"categories", component: CategoriesComponent},
  {path:"search/:search", component: SearchResultComponent},
  {path:"favorites/:favorites", component: FavoritesComponent},
  {path:"testimonies", component: TestimoniesComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
