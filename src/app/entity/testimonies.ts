export interface Testimonies {
    id?:number;
    title:string;
    content:string;
    imgPath:string;
}