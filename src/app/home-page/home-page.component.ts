import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../entity/product';
import { ProductService } from '../services/product.service';
import { Testimonies } from '../entity/testimonies';
import { HttpUrlEncodingCodec } from '@angular/common/http';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  searchSelected:string = '';
  searchText:string = '';
  products:Product[] = [];

  testimonies:Testimonies = {
    title:'',
    content:'',
    imgPath:''
};

  constructor(private router:Router, private productService:ProductService) {
   
  }
  // This function is used, when a user is typing a product, it will look for a matching product name in the database
  public filterProduct(value: string){
   this.productService.findByName(value).subscribe(data => this.products = data);
   
    
  }

  ngOnInit() {
    
  }
  // This function will navigate and encode the matching product name in the route url
  searchByName(){
    this.router.navigate(['/search', encodeURIComponent(this.searchText)]);

  }
}
